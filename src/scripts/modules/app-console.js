/**
 * Adjust styles of internal console function.
 */
'use strict';

class AppConsole {
  constructor() {
    this.styles = `font-size: 14px;font-weight: 500; padding: .4rem; `;
    this.debug({
      msg: `AppConsole initialized`
    })
  }
  /**
   * Information message.
   * @param {string} msg Intended output message.
   */
  log(msg) {
    return console.log( `%c${msg}`, `${this.styles} background-color: #f9f9fa; color: #343434`);
  }
  /**
   * Error messages.
   * @param {object} error Error object
   */
  error(error) {
    return console.error( `%c${error.message}`, `${this.styles} background-color: #ff0039 ; color: #ffffff`);
  }
  /**
   *
   * @param {string} msg Output debug message.
   */
  debug(msg) {
    if (window.APP_DEBUG) {
      if (typeof msg === 'object') {
        msg = `${JSON.stringify(msg)}`;
      };
      return console.log( `%c${msg}`, `${this.styles} background-color: #ffe900; color: #363636`);
    }
  }
}

export default AppConsole;
