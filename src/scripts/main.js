/**
 * Entry point of the application.
 */
'use strict';
import '../scss/main.scss';
import AppConsole from './modules/app-console';
const note = new AppConsole();

window.addEventListener('DOMContentLoaded', function(){
  note.log('Hello from App console');
  note.debug('Testing debug console styles');
})
