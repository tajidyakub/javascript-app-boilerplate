/**
 * Define the User Interface.
 */
'use strict';

import AppConsole from './scripts/modules/app-console.js';

const note = new AppConsole();
const APP_DEBUG = true;
let UI = { app: {}, sidebar: {}, main: {}};
/** Globals */
window.APP_DEBUG = APP_DEBUG;
window.UI = UI;


const sidebar = (context) => {
  return `
    <div class="sidebar bg-dark" id="Sidebar">
      <!-- Sidebar -->
    </div>`;
}

const main = (context) => {
  return `
    <div class="main" id="Main">
      <h1 class="page-title">${context.app.title}</h1>
      <!-- Main Content -->
    </div>
  `
}

const app = (context) => {
  return `
    <div class="wrapper" id="App">
      ${context.app.sidebar(context)}
      ${context.app.main(context)}
    </div>
  `
};

const context = {
  app: {
    title: `<strong>Javascript</strong> App boilerplate`,
    sidebar: sidebar,
    main: main
  }
};

const insertApp = () => {
  return document.body.innerHTML = app(context);
}


insertApp();

document.addEventListener('DOMContentLoaded', () => {
  if (APP_DEBUG) {
    note.debug({ debug: 'APP_DEBUG is true' });
  }
  try {
    UI.app = document.getElementById('App');
    UI.sidebar = document.getElementById('Sidebar');
    UI.main = document.getElementById('Main')
    return note.debug({ debug: 'UI Populated', context: [UI]});
  } catch (err) {
    return note.error(err);
  }
});


