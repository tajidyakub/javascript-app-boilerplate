const Html = require('html-webpack-plugin');
const Clean = require('clean-webpack-plugin');
const ExtractCss = require('mini-css-extract-plugin');
const path = require('path');

module.exports = {
  entry: {
    index: './src/index.js',
    main: './src/scripts/main.js'
  },
  output: {
    path: path.resolve(__dirname,'dist'),
    filename: 'scripts/[name].bundled.js'
  },
  devServer: {
    publicPath: '/',
    port: 45453 },
  devtool: 'source-map',
  stats: {
    // copied from `'minimal'`
    all: false,
    modules: true,
    maxModules: 0,
    errors: true,
    warnings: true,
    // our additional options
    moduleTrace: true,
    errorDetails: true
  },
  plugins: [
    new Clean(),
    new ExtractCss({
      filename: 'styles/[name].css',
      chunkFilename: 'styles/[id].css'
    }),
    new Html({ base: '/', hash: true })
  ],
  module: {
    rules: [
      { test: /\.js$/,
        exclude: [
          /node_modules/,
          /public/],
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-env'] }
        }
      },{
        test: /\.(woff|woff2|eot|ttf|otf)$/,
        use: [{
          loader: 'file-loader',
          options: {
            name: 'fonts/[name].[ext]'
          }
        }]
      },{
        test: /\.scss$/,
        use: [
          'style-loader',
          ExtractCss.loader,
          'css-loader',
          'sass-loader',
        ]
      }

    ]
  }
}
