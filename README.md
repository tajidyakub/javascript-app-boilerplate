# Javascript App boilerplate

> Kickstart for Javascript's app development with webpack4 preconfigured to work with sass/scss, postcss and ES6 modules format.

## Usage

View `package.json` scripts section for complete references, some of the useful commands are:

- `npm start` Bundle your assets and watch for changes while spawns Webpack Development server accessible via http://localhost:45453

- `npm run dev` Bundle javascript and scss assets and put the result iside `dist` folder

- `npm run dev:watch` Same as `npm start`

## Workflow

### Javascripts

A boilerplate of `src/scripts` with `modules` subfolder already created with the example of `app-consoles` modules;

Entry point for webpack is `src/scripts/index.js` which creates `index.html` and defines `global` variables.

You work on your app from the `src/scripts` as multiple module files with only `main.js`, and `import` form it. Or you can create multiple file in the `scripts` folder but be sure to assign a new value in `entry` property for `webpack` includes the files in the bundling process.

``` javascript
// webpack.config.js
entry: {
  index: '..',
  main: '...',
  yourfile: '..',
}
```

### SCSS

Since the `scss` files were imported from javascript files and extracted after compilation you can also develop your styles in separated modular `scss` file and `import` them from a contextual javascript file. The webpack configuration file also have to be modified, adding the name of the extracted files in the `plugins` property.
